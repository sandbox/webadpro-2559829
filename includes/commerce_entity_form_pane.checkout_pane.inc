<?php

/**
 * Implements the callback of the checkout pane form setting.
 */
function commerce_entity_form_pane_panes_contents_settings_form($checkout_pane) {
  $form = array();
  return $form;
}

/**
 * Implements the callback of the checkout pane form
 */
function commerce_entity_form_pane_panes_contents_checkout_form(&$form, &$form_state, $checkout_pane, $order) {
  $pane_form = array('#parents' => array($checkout_pane['pane_id']));
  $form_panes = variable_get('commerce_entity_form_pane', array());

  // Extract the entity_type & bundle
  $entity_type__bundle = substr($checkout_pane['pane_id'], strlen('commerce_entity_form_pane__'));
  list($entity_type, $bundle) = explode("__", $entity_type__bundle);

  $entity = null;
  if (!empty($order->data['commerce_entity_form_pane'][$entity_type__bundle])) {
    $entities = entity_load($entity_type, array($order->data['commerce_entity_form_pane'][$entity_type__bundle]));
    $entity = reset($entities);
  }

  $form_state['#commerce_entity_form_pane'] = array(
    'entity_type' => $entity_type,
    'bundle' => $bundle,
  );

  // Create a new entity of the specified type if it hasn't already been made.
  if (empty($entity)) {
    $entity = entity_create($entity_type, array('type' => $bundle));
  }

  $pane_form[$entity_type__bundle . '__entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $entity->{'#commerce_entity_form_pane'} = $form_panes[$entity_type][$bundle];

  // Add the field widgets for the profile.
  field_attach_form($entity_type, $entity, $pane_form, $form_state);

  return $pane_form;
}

/**
 * Implements the callback for the checkout pane form validate
 */
function commerce_entity_form_pane_panes_contents_checkout_form_validate($form, &$form_state, $checkout_pane, $order) {
  $entity_type = $form_state['#commerce_entity_form_pane']['entity_type'];
  $bundle = $form_state['#commerce_entity_form_pane']['bundle'];

  $entity = $form_state['values'][$checkout_pane['pane_id']][$entity_type . '__' . $bundle . '__entity'];

  // Notify field widgets to validate their data.
  field_attach_form_validate($entity_type, $entity, $form[$checkout_pane['pane_id']], $form_state);

  return TRUE;
}

/**
 * Implements the callback for the checkout pane form submit
 */
function commerce_entity_form_pane_panes_contents_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  $entity_type = $form_state['#commerce_entity_form_pane']['entity_type'];
  $bundle = $form_state['#commerce_entity_form_pane']['bundle'];

  $entity = $form_state['values'][$checkout_pane['pane_id']][$entity_type . '__' . $bundle . '__entity'];

  // Add some extra work for node titles.
  if ($entity_type == 'node') {
    $entity->title = $form_state['values'][$checkout_pane['pane_id']]['title'];
  }

  // Notify field widgets.
  field_attach_submit($entity_type, $entity, $form[$checkout_pane['pane_id']], $form_state);

  // Save the entity.
  entity_save($entity_type, $entity);

  list($entity_id) = entity_extract_ids($entity_type, $entity);

  // Store the entity ID for the related pane.
  $order->data['commerce_entity_form_pane'][$entity_type . '__' . $bundle] = $entity_id;
}

/**
 * Checkout pane callback: returns the cart contents review data for the
 *   Review checkout pane.
 */
function commerce_entity_form_pane_panes_contents_review($form, $form_state, $checkout_pane, $order) {
  $pane_form = array('#parents' => array($checkout_pane['pane_id']));

  // Extract the entity_type & bundle
  $entity_type__bundle = substr($checkout_pane['pane_id'], strlen('commerce_entity_form_pane__'));
  list($entity_type, $bundle) = explode("__", $entity_type__bundle);

  $entities = entity_load($entity_type, array($order->data['commerce_entity_form_pane'][$entity_type . '__' . $bundle]));
  $entity = reset($entities);

  list($entity_id) = entity_extract_ids($entity_type, $entity);
  $content = entity_view($entity_type, array($entity_id => $entity), 'commerce_entity_form_pane', NULL, TRUE);

  return drupal_render($content);
}
